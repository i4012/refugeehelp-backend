'use strict'

var express = require('express');
var router = express.Router();
var Points = require('../models/point');

const rabbit = require('amqplib');

const QUEUE_NAME = 'points';
const EXCHANGE_TYPE = 'direct';
const EXCHANGE_NAME = 'main';

const { createLogger, transports, format } = require("winston");
const LokiTransport = require('winston-loki')
const logger = createLogger()

logger.add(new transports.Console({
    format: format.json(),
    level: 'debug'
}));


logger.add(new LokiTransport({
    host: 'http://loki:3100',
    json: true,
    labels: { job: 'backend' },
    level: 'debug'
}));

// htttps://localhost:5000/points/
router.get('/', (req, res) => {
    try {
        Points.find({}).then( points => {
            res.send(points);
        });
        logger.info("Points get");
    } catch (err) {
        logger.error("Points get");
        res.status(500).send('500');
    }
});

router.post('/', (req, res) => {
    console.log("creez punct public");
    const email = req.body.authorEmail;
    const lat = req.body.lat;
    const lng = req.body.lng;
    const icon = req.body.icon;
    const title = req.body.title;
    const description = req.body.description;
    if (!email || !lat || !lng || !title || !description) {
        console.log("invalid body");
        logger.error("Points post");
        res.status(400);
        res.send({"Message" : "Please fill all fields"});
    }
    else if (isNaN(lat) || isNaN(lng)) {
        console.log("lat or lng is not a number");
        logger.error("Points post");
        res.status(400);
        res.send({"Message" : "Latitude or Longitude is not a number"});
    }
    else if (lat < -90 || lat > 90) {
        console.log("lat is not in range");
        logger.error("Points post");
        res.status(400);
        res.send({"Message" : "Latitude is not in range -90 to 90"});
    }
    else if (lng < -180 || lng > 180) {
        logger.error("Points post");
        console.log("lng is not in range");
        res.status(400);
        res.send({"Message" : "Longitude is not in range -180 to 180"});
    }
    else {
        let newEntryData = req.body;
        console.log("creating new entry");

        Points.create(newEntryData, (err, db) => {
            if (err) {
                logger.error("Points post");
                console.log("not created");
                console.log(err);
                res.status(400);
                res.send({"Message" : "Invalid body"});

            } else {

                logger.info("Points post");
                console.log("created");
                
                let connection = rabbit.connect('amqp://rabbit');
                connection.then(async (conn)=>{
                    const KEY = 'myKey';
                    const channel = await conn.createChannel();
                    console.log("channel created");
                    await channel.assertExchange(EXCHANGE_NAME, EXCHANGE_TYPE, {durable: false});
                    channel.bindQueue(QUEUE_NAME, EXCHANGE_NAME, KEY);
                    channel.sendToQueue(QUEUE_NAME, Buffer.from(req.body.title));
                });

                res.status(200);
                res.send({"Message" : "Entry added"});
            }
        })
    }
});

router.delete('/:id', (req, res) => {
    const id = req.params.id;
    Points.findByIdAndDelete(id, (err, db) => {
        if (err) {
            logger.error("Points delete");
            console.log(err);
            res.status(400);
            res.send({"Message" : "Invalid id"});
        } else {
            console.log("deleted");
            logger.info("Points delete");
            console.log(db);
            res.status(200);
            res.send({"Message" : "Entry deleted"});
        }
    });
});

router.delete('/', (req, res) => {
    Points.findOneAndDelete({title : req.body.title}, (err, removeSuccess) => {
        if (err) {
            logger.error("Points delete");
            console.log(err);
        }
        if (!removeSuccess) {
            logger.error("Points delete");
            res.status(404);
            console.log(`No entry with the name:${req.body.title}`);
            res.send({"Message" : `No entry with the name:${req.body.title}`});
        } else {
            logger.info("Points delete");
            res.status(200);
            res.send({"Message" : "Entry deleted"});
        }
     })
});

router.put('/', (req, res) => {

});

module.exports = router
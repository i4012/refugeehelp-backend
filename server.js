'use strict'

const express = require('express');
const mongoose = require('mongoose');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const app = express();
const port = 5000;
const pointRouter = require('./routes/pointRouter');
const userRouter = require('./routes/userRouter');
const emailRouter = require('./routes/emailRouter');
const cors = require('cors');

const { createLogger, transports, format } = require("winston");
const LokiTransport = require('winston-loki')
const logger = createLogger()

logger.add(new transports.Console({
    format: format.json(),
    level: 'debug'
}));


logger.add(new LokiTransport({
    host: 'http://loki:3100',
    json: true,
    labels: { job: 'backend' },
    level: 'debug'
}));

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://database:27017/refugeedb', { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;

db.on('error', (error) => logger.error(error));
db.once('open', () => logger.debug({message:"DB connected", tags: {someCustomTag:"mongoose"}}));

app.use(express.static('public'));
app.use(express.json());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(cors());

app.use('/points', pointRouter);
app.use('/users', userRouter);
app.use('/emails', emailRouter);

app.listen(port, () => logger.debug('Server up and serving'));

